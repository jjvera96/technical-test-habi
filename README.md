# Tecnical Test Habi (Prueba Tecnica de Habi)

Python project to solve the technical test of Habi. (Proyecto python para resolver la prueba tecnica de Habi.)

## Query Service (Servicio Consulta)

Short documentation of the query service. (Pequeña documentacion para el servicio de consulta.)

### Tecnology To Develop (Tecnologia Para Desarrollar)

- **Python** Lenguage to develop. (Lenguaje de desarrollo.)
- **MySQL** Database management system. (Sistema gestor de base de datos.)
- **Postman** App to send petitions and document the code. (App para enviar peticiones y documentar el codigo.)
- **SQL** Lenguage to make queries to database. (Lenguajep para hacer consultas a la base de datos.)

### Project Structure (Estructura del Proyecto)

```bash
.
    ├── api
        ├── config
            ├── __init__.py
            ├── db.py
            ├── settings.py
        ├── tests
            ├── __init__.py
            ├── test_property_api.py
            ├── test_property_url.py
        ├── urls
            ├── __init__.py
            └── property.py
        ├── utils
            ├── __init__.py
            ├── defaults.py
            ├── filter_params.py
            ├── serializer_object.py
            └── validator_params_property.py
        ├── views
            ├── __init__.py
            └── property.py
        ├── .env
        └── server.py
        └── test.py
    ├── documentation
    ├── env
    ├── .gitignore
    └── README.md
```

### Steps to solve the test (Pasos Para Resolver La Prueba)

- Connection to db. (Conexión a la base de datos.)

- Identify the models. (Identificar los modelos.)

- Design diagrams of models to identify the requirements. (Diseñar los diagramas de los modelos para identificar los requerimientos.)

- Create a basic server to receive GET and POST requests. (Crear aun servidor basico para recibir peticiones GET y POST.)

- Create endpoints for getting requests. (Crear puntos finales para recibir peticiones.)

- Create filters for requests. (Crear filtros para las peticiones.)

- Crate unit test. (Crear pruebas unitarias.)

- Document code and api. (Documentar codigo y api.)

## First ER Diagram (Primer Diagrama Entidad Relación)

![Alt text](documentation/er_diagram_query.png "ER Diagram")

### Installation (Instalación)

To run this project you need to install Python. Also, when you clone the project, you should create a .env file. In the .env file you have to add the variables environment. (Para correr este proyecto se necesita instalar Python. Tambien, cuando se clone el proyecto, se debera crear un archivo .env. En este archivo se deben agregar variables de entorno.)

Example of .env file (Ejemplo de archivo .env.)

```bash
HOST=ip
PORT=port
USER=dbuser
PASSWORD=dbpassword
SCHEMA=dbschema
```

- Clone the project and access the folder. (Clonar el proyecto y acceder a la carpeta)

```bash
git clone git@gitlab.com:jjvera96/technical-test-habi.git && cd technical-test-habi
```

- Create the virtual environment. (Crear el entorno virtual.)

```bash
python -m venv env
```

- Activate To *Windows* (Activar el entorno virtual para Windows.)

```bash
.env/Scripts/activate
```

- Activate To *Linux* (Activar el entorno virtual para Linux.)

```bash
.env/bin/activate
```

- Install requirements (Instalar requirements de Python.)

```bash
pip install -r requirements.txt
```

- Access to api folder (Acceder a la carpeta de la API.)

```bash
cd api
```

- Run the server (Correr el servidor.)

```bash
python server.py
```

### Test

To test the project, run the following command. You must be in api folder. (Para correr los test, se debe correr el siguiente comando. Estar en la carpeta api.)

```bash
pytest tests
```

### Documentation on Postman

[Go documentation (Ir a la documentación)](https://documenter.getpostman.com/view/16607189/UzXVuuaB)

### To Consider (Para Considerar)

Some registers do not have price, description or year, then, to make the queries they are discarted. This is done for usability. (Algunos registros no tienen precio, descripción o año, entonces, para hacer las consultas son descartados. Esto es hecho por usabilidad.)

Also, we can put some kind of authentication and permissions for having access to the services. (Tambien, podemos colocar algun tipo de autenticación y permisos para tener acceso a los servicios.)

## Like Service (Servicio Me Gusta)

Registered users can give "Like" to a lot of properties. Therefore, we need to create a table where store the registers of "Like" of the users to properties. (Los usuarios registardos pueden dar "Me gusta" a muchas propiedaes. De este modo, necesitamos crear una tabla donde almacenemos los registros de los "Me gusta" de los usuarios a las propiedades.)

### Second ER Diagram  (Segundo Diagrama Entidad Relación)

![Alt text](documentation/er_diagram_like.png "ER Diagram")

### SQL To Create New Tables (SQL Para Crear Nuevas Tablas)

```bash
CREATE TABLE user (
    id INT(11) AUTO_INCREMENT,
    first_name VARCHAR(64) NOT NULL,
    last_name VARCHAR(64) NOT NULL,
    dni INT(64) NOT NULL,
    email VARCHAR(255) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE like (
    id INT(11) AUTO_INCREMENT,
    user_id INT(11) NOT NULL REFERENCES user (id) ON DELETE CASCADE,
    property_id INT(11) NOT NULL REFERENCES property (id) ON DELETE CASCADE,
    created TIMESTAMP,
    PRIMARY KEY (id)
);
```

### SQL To Queries (SQL Para Consultas)

- Create Like. (Crear Me Gusta)

```bash
INSERT INTO like_history (user_id, property_id, created)
VALUES ('1','1',STR_TO_DATE("28-08-2022 10:20:10", "%m-%d-%Y %H:%i:%s"));
```

- Query of like to the properties made by a user. (Consulta de Me gusta a las propiedades hechos por un usuario.)

```bash
SELECT P.address, P.city, P.description, P.price, P.year  
FROM property P
JOIN like L
ON P.id = L.property_id
WHERE L.id_user = '{user_id}';
```

- Query of the properties with more like. (Consulta de las propiedades con mas Me Gusta.)

```bash
SELECT DISTINCT L.property_id, count(L.property_id) as likes, P.description, P.price, P.address, P.city, P.year 
FROM like L
JOIN property P
ON L.property_id = P.id
GROUP BY property_id
ORDER BY likes DESC;
```
