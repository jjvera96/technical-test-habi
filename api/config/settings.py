""" Contains params of db connection """
import os
from dotenv import load_dotenv
load_dotenv()

SCHEMA = os.getenv('SCHEMA')
HOST = os.getenv('HOST')
PORT = os.getenv('PORT')
USER = os.getenv('USER')
PASSWORD = os.getenv('PASSWORD')

DB_PARAMS = {
    "schema": SCHEMA, 
    "host": HOST, 
    "port": PORT, 
    "user": USER, 
    "password": PASSWORD
}
