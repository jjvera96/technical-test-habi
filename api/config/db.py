""" Contains class Database for connecting to db and making queries """
import pymysql
from .settings import DB_PARAMS

class Database:
    def __init__(self):
        try:
            self.connection = pymysql.connect(
                host=DB_PARAMS['host'],
                user=DB_PARAMS['user'],
                password=DB_PARAMS['password'],
                db=DB_PARAMS['schema'],
                port=int(DB_PARAMS['port']),
                cursorclass=pymysql.cursors.DictCursor
            )
            self.cursor = self.connection.cursor()
        except:
            raise Exception("Unable to connect to database")

    def get_tables(self):
        query = 'SHOW TABLES;'
        self.cursor.execute(query)
        records = self.cursor.fetchall()
        return records

    def get_columns_table(self, title):
        records = 'SHOW COLUMNS FROM {};'.format(title)
        self.cursor.execute(records)
        records = self.cursor.fetchall()
        return records

    def close(self):
        self.connection.close()

database_connect = Database()


