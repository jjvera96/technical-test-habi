""" Contains a function that serializer an property object """

def serializer_object(object):
    response = {
        "id": object["id"],
        "city": object["city"],
        "year": object["year"],
        "price": object["price"],
        "address": object["address"],
        "label_state": object["label"],
        "description": object["description"],
    }
    return response
