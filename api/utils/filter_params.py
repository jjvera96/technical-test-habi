""" Contains a function that helps filter"""

def filter_params(query_params):
    filter_by_query = ""
    if "year" in query_params:
        query_params_prep = "','".join(query_params["year"].split(","))
        filter_by_query += " AND P.year in ('{}')".format(query_params_prep)
    if "city" in query_params:
        query_params_prep = "','".join(query_params["city"].split(","))
        filter_by_query += " AND P.city in ('{}')".format(query_params_prep)
    if "state" in query_params:
        query_params_prep = "','".join(query_params["state"].split(","))
        filter_by_query += " AND S.name in ('{}')".format(query_params_prep)
    else:
        filter_by_query += " AND S.name in ('vendido', 'en_venta', 'pre_venta')"
    return filter_by_query

