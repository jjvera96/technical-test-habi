""" Contains default values """

URL_RESPONSE_DEFAULT = {
    'status_code': 400,
    'response': {
        'code': 'invalid_path',
        'error': 'Invalid path.'
    }
}

INVALID_METHOD_RESPONSE_DEFAULT = {
    'status_code': 400,
    'response': {
        'code': 'invalid_method',
        'error': 'Invalid method.'
    }
}

VIEW_RESPONSE_DEFAULT = {
    'status_code': 400,
    'response': {
        'code': 'bad_request', 
        'error': 'Invalid data.'
    }
}

DATABASE_ERROR_RESPONSE =  {
    'status_code': 500,
    'response': {
        'code': 'database_error',
        'error': 'There was an error while loading data.'
    }
}