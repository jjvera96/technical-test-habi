""" Contains a function that validate an object to create property """

def validator_params_property(params):
    keys = ["address", "city", "price", "description", "year"]
    for key in keys:
        if not key in params:
            return False
        else:
            if params[key] is None:
                return False
    return True
