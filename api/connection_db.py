""" Contains a connection to db for getting information about the database and saving it in json file"""
from config.db import database_connect
import json

def main():
    data = {}
    tables = database_connect.get_tables()
    for table in tables:
        columns = database_connect.get_columns_table(table['Tables_in_habi_db'])
        data[table['Tables_in_habi_db']] = columns
    with open('db.json', 'w') as file:
        json.dump(data, file, indent=4)
    database_connect.close()

if __name__ == "__main__":
    main()
