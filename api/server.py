""" Contains definition of server """
import json
from urllib.parse import urlparse
from urls.property import PropertyURL
from http.server import BaseHTTPRequestHandler
from http.server import HTTPServer
from utils.defaults import URL_RESPONSE_DEFAULT
from utils.defaults import INVALID_METHOD_RESPONSE_DEFAULT


class Server(BaseHTTPRequestHandler):
    def do_HEAD(self):
        self.send_header('Content-type', 'application/json')
        self.end_headers()

    def invalid_request(self):
        self.send_response(URL_RESPONSE_DEFAULT['status_code'])
        self.do_HEAD()
        self.wfile.write(json.dumps(URL_RESPONSE_DEFAULT['response']).encode('utf-8'))

    def invalid_method(self):
        self.send_response(INVALID_METHOD_RESPONSE_DEFAULT['status_code'])
        self.do_HEAD()
        self.wfile.write(json.dumps(INVALID_METHOD_RESPONSE_DEFAULT['response']).encode('utf-8'))

    do_PUT = invalid_method
    do_DELETE = invalid_method

    def do_GET(self):
        request_path = self.path
        query_params = urlparse(request_path).query
        query_params_dict = {}
        if len(query_params) > 0:
            query_params_dict = dict(qc.split("=")for qc in query_params.split("&"))
        if request_path.startswith('/property'):
            if '?' in request_path:
                complement_path = request_path.split('?')[0].lstrip("/property/")
            else:
                complement_path = request_path.lstrip("/property/")
            response = PropertyURL.get(complement_path, query_params_dict)
            self.send_response(response["status_code"])
            self.do_HEAD()
            self.wfile.write(json.dumps(
                response["response"]).encode('utf-8'))
        else:
            return self.invalid_request()

    def do_POST(self):
        request_path = self.path
        length = int(self.headers.get('Content-Length'))
        if length > 0:
            body = json.loads(self.rfile.read(length))
        if request_path.startswith('/property'):
            if '?' in request_path:
                complement_path = request_path.split('?')[0].lstrip("/property/")
            else:
                complement_path = request_path.lstrip("/property/")
            response = PropertyURL.post(
                complement_path, body)
            self.send_response(response['status_code'])
            self.do_HEAD()
            self.wfile.write(json.dumps(
                response['response']).encode('utf-8'))
        else:
            return self.invalid_request()
    
def main():
    port = 8000
    print('Listening on {}'.format(port))
    server = HTTPServer(('', port), Server)
    server.serve_forever()

if __name__ == '__main__':
    main()
