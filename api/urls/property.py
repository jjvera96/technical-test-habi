""" Contains Property URL """
from views.property import PropertyApi
from utils.defaults import URL_RESPONSE_DEFAULT
import re


class PropertyURL:
    def get(complement_path, params):
        if complement_path == '':
            return PropertyApi.list(params)
        result = re.search(r'^(?P<property>[0-9]+)', complement_path)
        if result: 
            return PropertyApi.detail(result.group('property'))
        return URL_RESPONSE_DEFAULT

    def post(complement_path, body):
        if complement_path == '':
            return PropertyApi.create(body)
        return URL_RESPONSE_DEFAULT
