""" Contains PropertyAPI endpoint"""
from config.db import database_connect
from datetime import datetime
from utils.filter_params import filter_params
from utils.serializer_object import serializer_object
from utils.validator_params_property import validator_params_property
from utils.defaults import VIEW_RESPONSE_DEFAULT
from utils.defaults import DATABASE_ERROR_RESPONSE


class PropertyApi:
    def list(query_params):
        if query_params:
            filter_by_query = filter_params(query_params)
        else:
            filter_by_query = " AND S.name in ('vendido', 'en_venta', 'pre_venta')"
        query = """
            SELECT P.id, P.address, P.city, S.label, P.price, P.description, P.year
            FROM property P
            JOIN status_history SH
            ON P.id = SH.property_id
            JOIN status S
            ON S.id = SH.status_id
            WHERE P.description != 'None' AND P.price != 'None' AND P.year is not null
            {} 
            AND Exists(
                SELECT 1
                FROM   status_history
                WHERE  property_id = SH.property_id
                HAVING SH.update_date = Max(update_date)
            )
            ORDER BY P.year
        """.format(filter_by_query)
        try:
            database_connect.cursor.execute(query)
            records = database_connect.cursor.fetchall()
        except:
            return DATABASE_ERROR_RESPONSE
        response = [serializer_object(record) for record in records]
        return {
            'status_code': 200, 
            'response': response
        }

    def create(body):
        response = VIEW_RESPONSE_DEFAULT
        if not validator_params_property(body):
            return response
        else:
            try:
                query = """
                    INSERT INTO property (address, city, price, description, year)
                    VALUES ('{}', '{}', '{}', '{}', {})
                """.format(body["address"], body["city"],body["price"],body["description"],body["year"])
                database_connect.cursor.execute(query)

                query = """
                    INSERT INTO status_history (property_id, status_id, update_date)
                    VALUES ('{}', '3', '{}')
                """.format(database_connect.cursor.lastrowid, datetime.now())
                database_connect.cursor.execute(query)
                database_connect.connection.commit()
                response = {"response": {"message": "property created"}, "status_code": 201}
            except:
                return DATABASE_ERROR_RESPONSE
        return response

    def detail(id):
        query = """
            SELECT P.id, P.address, P.city, S.label, P.price, P.description, P.year
            FROM property P
            JOIN status_history SH
            ON P.id = SH.property_id
            JOIN status S
            ON S.id = SH.status_id
            WHERE P.id={}
        """.format(id)
        try:
            database_connect.cursor.execute(query)
            register = database_connect.cursor.fetchone()
        except Exception as e:
            return DATABASE_ERROR_RESPONSE
        if register:
            return {
                'status_code': 200, 
                'response': serializer_object(register)}
        else:
            return {
                'status_code': 400,
                'response': 'Not found.'
            }

        
    
