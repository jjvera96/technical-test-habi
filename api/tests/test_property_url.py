""" Contains tests of PropertyURL """
from urls.property import PropertyURL
from utils.defaults import URL_RESPONSE_DEFAULT

class TestPropertyURL:
    def test_get_list(self):
        complement_path = ''
        query_params = {
           'year': '2000,',
           'city': 'bogota,',
           'state': 'pre_venta,' 
        }
        response = PropertyURL.get(complement_path, query_params)
        assert 'status_code' in response
        assert response['status_code'] == 200
        assert response['response'][0]['year'] == 2000
        assert response['response'][-1]['year'] == 2000

    def test_get_list_nodata(self):
        complement_path = ''
        query_params = {
           'year': '1754,',
           'city': 'macedonia,',
           'state': 'pre_venta,' 
        }
        response = PropertyURL.get(complement_path, query_params)
        assert 'status_code' in response
        assert response['status_code'] == 200
        assert len(response['response']) == 0

    def test_get_id(self):
        complement_path = '58'
        query_params = {}
        response = PropertyURL.get(complement_path, query_params)
        assert 'status_code' in response
        assert response['status_code'] == 200
        assert response['response']['id'] == 58

    def test_get_id_fail(self):
        complement_path = '16897'
        query_params = {}
        response = PropertyURL.get(complement_path, query_params)
        assert response['status_code'] == 400
        assert response['response'] == 'Not found.'

    def test_detail_fail_url(self):
        complement_path = 'afasf'
        query_params = {}
        response = PropertyURL.get(complement_path, query_params)
        assert response == URL_RESPONSE_DEFAULT