""" Contains tests of PropertyApi """
from views.property import PropertyApi
from utils.defaults import DATABASE_ERROR_RESPONSE


class TestPropertyApi:
    def test_list(self):
        query_params = {
           'year': '2000,',
           'city': 'bogota,',
           'state': 'pre_venta,' 
        }
        response = PropertyApi.list(query_params)
        assert 'status_code' in response
        assert response['status_code'] == 200
        assert response['response'][0]['year'] == 2000
        assert response['response'][-1]['year'] == 2000

    def test_list_nodata(self):
        query_params = {
           'year': '1754,',
           'city': 'macedonia,',
           'state': 'pre_venta,' 
        }
        response = PropertyApi.list(query_params)
        assert 'status_code' in response
        assert response['status_code'] == 200
        assert len(response['response']) == 0

    def test_detail_ok(self):
        response = PropertyApi.detail(58)
        assert response['status_code'] == 200
        assert response['response']['id'] == 58
        
    def test_detail_fail(self):
        response = PropertyApi.detail(16897)
        assert response['status_code'] == 400
        assert response['response'] == 'Not found.'

    def test_detail_fail_db(self):
        response = PropertyApi.detail('afasf')
        assert response == DATABASE_ERROR_RESPONSE
